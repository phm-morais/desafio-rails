Rails.application.routes.draw do
  devise_for :users
  root to: 'clients#index'
  resources :clients
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
