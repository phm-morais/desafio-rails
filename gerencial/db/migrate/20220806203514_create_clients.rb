class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :nome
      t.integer :conta
      t.string :senha
      t.string :email
      t.string :mensagem

      t.timestamps
    end
  end
end
