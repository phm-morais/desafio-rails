class User < ApplicationRecord
  attr_accessor :conta
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  has_many :clients

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
      
end
